<h1 lang="fa" dir="rtl" align="right">دیپلوی نرم افزار بدون داون تایم</h1>
<p lang="fa" dir="rtl" align="right">
ما اینجا ۳ رول برای کانفیگ سرور و بیلد ایمیج و دیپلوی ایمیج روی سرور داریم
همه پروسس ها روی سرور که داخل فایل inventory قرار داره انجام میشود.

با دستور زیر کلید عمومی رو روی سرور قرار می‌دهیم
</p>

```
ssh-copy-id [user]@[ip] -p [port]
```
<p lang="fa" dir="rtl" align="right">
داخل فایل inventory.cnf آدرس سرور مورد نظر رو قرار می‌دهیم.
</p>

<p lang="fa" dir="rtl" align="right">
۲ تا فایل server_config.yml و run.yml به ترتیب برای کانفیگ سرور(نصب داکر و انجینکس) و دومی برای بیلد و دیپلوی ایمیج روی سرور استفاده می‌شوند.
</p>

<p lang="fa" dir="rtl" align="right">
برای اجرا کردن پروسس کافیه دو دستور زیر رو وارد کنیم
</p>

```
ansible-playbook -i inventory.cnf server_config.yml
ansible-playbook -i inventory.cnf run.yml
```

<p lang="fa" dir="rtl" align="right">
برای تغییر ورژن کافیه وریبل app_version داخل فایل run.yml رو تغییر بدیم.
</p>